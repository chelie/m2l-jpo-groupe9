﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace jpo
{
    public partial class frmEnregistrementLigues : Form
    {
        static List<String> listeLigue = new List<String>();
        public frmEnregistrementLigues()
        {
            InitializeComponent();
            AutoValidate = AutoValidate.Disable;
        }

        private void frmEnregistrementLigues_Load(object sender, EventArgs e)
        {

            // Désactivation des boutons Ajouter , modifier , supprimer
            btnModifier.Enabled = false;
            btnSupprimer.Enabled = false;
            tbxNomLigue.Enabled = false;
            tbxAdresse.Enabled = false;
            tbxCodePostal.Enabled = false;
            tbxDiscipline.Enabled = false;
            tbxVille.Enabled = false;

            loadLigues();
        }

        private void lbLigues_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnModifier.Enabled = true;
            btnSupprimer.Enabled = true;
            loadLiguesInfo();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            etatModif();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            frmAjoutLigues formAjt = new frmAjoutLigues();
            formAjt.FormClosed += new FormClosedEventHandler(formAjt_Closed);
            formAjt.Show();
        }

        private void formAjt_Closed(object sender, FormClosedEventArgs e) {
            loadLigues();
        }

        private void tbxNomLigue_Validating(object sender, CancelEventArgs e)
        {
            if(tbxNomLigue.Text == "")
            {
                this.errorProvider1.SetError(tbxNomLigue, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxAdresse_Validating(object sender, CancelEventArgs e)
        {
            if (tbxAdresse.Text == "")
            {
                this.errorProvider1.SetError(tbxAdresse, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxCodePostal_Validating(object sender, CancelEventArgs e)
        {
            if (tbxCodePostal.Text == "")
            {
                this.errorProvider1.SetError(tbxCodePostal, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxVille_Validating(object sender, CancelEventArgs e)
        {
            if (tbxVille.Text == "")
            {
                this.errorProvider1.SetError(tbxVille, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxDiscipline_Validating(object sender, CancelEventArgs e)
        {
            if (tbxDiscipline.Text == "")
            {
                this.errorProvider1.SetError(tbxDiscipline, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            String sql = "delete from ligues where nomLigue ='" + lbLigues.GetItemText(lbLigues.SelectedItem) + "';";
            lbLigues.Items.Remove(lbLigues.SelectedItem);
            DbConnex.connexionBase();
            int cmd = DbConnex.nonQuery(sql);
            if (cmd != 1) { MessageBox.Show("Une erreur s'est produite, veuillez vérifier que le nom de ligue ou la discipline n'est pas déjà utilisé."); }
            else
            {
                clearInfo();
            }
            DbConnex.connexionClose();
        }
        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            lbLigues.Enabled = true; ;
            btnAnnuler.Visible = false;
            btnModifier.Visible = true;
            btnSupprimer.Visible = true;

            clearInfo();
        }

        private void btnConfirmModif_Click(object sender, EventArgs e)
        {
            if (lbLigues.Items.Contains(tbxNomLigue.Text) && tbxNomLigue.Text != lbLigues.Text)
            {
                MessageBox.Show("Cette ligue existe déjà");
            }
            else
            {
                String sql = "update ligues set nomLigue='" + tbxNomLigue.Text + "',adresse='" + tbxAdresse.Text + "',cp='" + tbxCodePostal.Text + "',ville='" + tbxVille.Text + "',discipline='" + tbxDiscipline.Text + "' WHERE nomLigue='"+lbLigues.Text +"'";
                DbConnex.connexionBase();
                DbConnex.nonQuery(sql);
                DbConnex.connexionClose();

                lbLigues.Enabled = true;
                btnAnnuler.Visible = false;
                btnModifier.Visible = true;
                btnConfirmModif.Visible = false;
                btnSupprimer.Visible = true;
                btnEnregistrer.Enabled = true;

                // Remise de base des boutons

                lbLigues.SelectedItems.Clear();
                btnSupprimer.Visible = true;
                btnModifier.Enabled = false;
                btnSupprimer.Enabled = false;

                tbxNomLigue.Enabled = false;
                tbxAdresse.Enabled = false;
                tbxCodePostal.Enabled = false;
                tbxDiscipline.Enabled = false;
                tbxVille.Enabled = false;

                clearInfo();
            }
        }
        private void tbxRecherche_TextChanged(object sender, EventArgs e)
        {
                lbLigues.Items.Clear();
                foreach (String s in listeLigue)
                {
                    string ligue = s;
                    if (ligue.Contains(tbxRecherche.Text.ToLower()))
                    {
                        lbLigues.Items.Add(s);
                    }
                }
        }

        private void tbxNomLigue_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxNomLigue.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxAdresse_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxAdresse.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void tbxCodePostal_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxCodePostal.MaxLength = 5;
            var regex = new Regex(@"[^0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void tbxVille_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxVille.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxDiscipline_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxDiscipline.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        //-------FONCTIONS----------------

        private void loadLigues()
        {
            lbLigues.Items.Clear();
            DbConnex.connexionBase();
            OleDbDataReader drLigues = DbConnex.GetDataReader("select * from ligues");
            while (drLigues.Read())
            {
                lbLigues.Items.Add(drLigues.GetString(1));
                listeLigue.Add(drLigues.GetString(1));
            }
            DbConnex.connexionClose();
        }

        private void loadLiguesInfo()
        {
            DbConnex.connexionBase();
            OleDbDataReader drLigues = DbConnex.GetDataReader("select adresse,cp,ville,discipline from ligues where nomLigue='" + lbLigues.GetItemText(lbLigues.SelectedItem) + "'");
            while (drLigues.Read())
            {
                try
                {
                    tbxNomLigue.Text = lbLigues.GetItemText(lbLigues.SelectedItem);
                    tbxAdresse.Text = Convert.ToString(drLigues[0]);
                    tbxCodePostal.Text = Convert.ToString(drLigues[1]);
                    tbxVille.Text = Convert.ToString(drLigues[2]);
                    tbxDiscipline.Text = Convert.ToString(drLigues[3]);
                }
                catch (InvalidCastException ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            DbConnex.connexionClose();
        }

        private void etatModif()
        {
            lbLigues.Enabled = false;
            btnAnnuler.Visible = true;
            btnModifier.Visible = false;
            btnConfirmModif.Visible = true;
            btnEnregistrer.Enabled = false;

            btnSupprimer.Hide();

            tbxNomLigue.Enabled = true;
            tbxAdresse.Enabled = true;
            tbxCodePostal.Enabled = true;
            tbxDiscipline.Enabled = true;
            tbxVille.Enabled = true;
        }
        private void clearInfo()
        {
            tbxNomLigue.Text = "";
            tbxAdresse.Text = "";
            tbxCodePostal.Text = "";
            tbxVille.Text = "";
            tbxDiscipline.Text = "";
        }
    }
}
