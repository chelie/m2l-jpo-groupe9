﻿
namespace jpo
{
    partial class frmEnregistrementMembres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbLigues = new System.Windows.Forms.ListBox();
            this.lbMembres = new System.Windows.Forms.ListBox();
            this.btnInscription = new System.Windows.Forms.Button();
            this.btnDesinscription = new System.Windows.Forms.Button();
            this.btnModification = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Noto Sans Cond", 36F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(134, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(539, 65);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enregistrement membres";
            // 
            // lbLigues
            // 
            this.lbLigues.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lbLigues.FormattingEnabled = true;
            this.lbLigues.ItemHeight = 26;
            this.lbLigues.Location = new System.Drawing.Point(26, 164);
            this.lbLigues.Name = "lbLigues";
            this.lbLigues.Size = new System.Drawing.Size(238, 264);
            this.lbLigues.TabIndex = 2;
            this.lbLigues.SelectedIndexChanged += new System.EventHandler(this.lbLigues_SelectedIndexChanged);
            // 
            // lbMembres
            // 
            this.lbMembres.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lbMembres.FormattingEnabled = true;
            this.lbMembres.ItemHeight = 26;
            this.lbMembres.Location = new System.Drawing.Point(295, 164);
            this.lbMembres.Name = "lbMembres";
            this.lbMembres.Size = new System.Drawing.Size(238, 264);
            this.lbMembres.TabIndex = 3;
            this.lbMembres.SelectedIndexChanged += new System.EventHandler(this.lbMembres_SelectedIndexChanged);
            // 
            // btnInscription
            // 
            this.btnInscription.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnInscription.Location = new System.Drawing.Point(589, 181);
            this.btnInscription.Name = "btnInscription";
            this.btnInscription.Size = new System.Drawing.Size(128, 49);
            this.btnInscription.TabIndex = 4;
            this.btnInscription.Text = "Ajouter";
            this.btnInscription.UseVisualStyleBackColor = true;
            this.btnInscription.Click += new System.EventHandler(this.btnInscription_Click);
            // 
            // btnDesinscription
            // 
            this.btnDesinscription.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnDesinscription.Location = new System.Drawing.Point(589, 248);
            this.btnDesinscription.Name = "btnDesinscription";
            this.btnDesinscription.Size = new System.Drawing.Size(128, 49);
            this.btnDesinscription.TabIndex = 5;
            this.btnDesinscription.Text = "Supprimer";
            this.btnDesinscription.UseVisualStyleBackColor = true;
            this.btnDesinscription.Click += new System.EventHandler(this.btnDesinscription_Click);
            // 
            // btnModification
            // 
            this.btnModification.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnModification.Location = new System.Drawing.Point(589, 313);
            this.btnModification.Name = "btnModification";
            this.btnModification.Size = new System.Drawing.Size(128, 50);
            this.btnModification.TabIndex = 6;
            this.btnModification.Text = "Modifier";
            this.btnModification.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(21, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 26);
            this.label2.TabIndex = 7;
            this.label2.Text = "Liste ligues";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(290, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Liste membres";
            // 
            // frmEnregistrementMembres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnModification);
            this.Controls.Add(this.btnDesinscription);
            this.Controls.Add(this.btnInscription);
            this.Controls.Add(this.lbMembres);
            this.Controls.Add(this.lbLigues);
            this.Controls.Add(this.label1);
            this.Name = "frmEnregistrementMembres";
            this.Text = "Enregistrement membres";
            this.Load += new System.EventHandler(this.frmEnregistrementMembres_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbLigues;
        private System.Windows.Forms.ListBox lbMembres;
        private System.Windows.Forms.Button btnInscription;
        private System.Windows.Forms.Button btnDesinscription;
        private System.Windows.Forms.Button btnModification;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}