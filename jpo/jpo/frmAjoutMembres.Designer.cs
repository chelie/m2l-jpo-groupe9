﻿namespace jpo
{
    partial class frmAjoutMembres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCreerMembre = new System.Windows.Forms.Button();
            this.lblVilleLigue = new System.Windows.Forms.Label();
            this.lblCodePostalLigue = new System.Windows.Forms.Label();
            this.lblAdresseLigue = new System.Windows.Forms.Label();
            this.lblNomLigue = new System.Windows.Forms.Label();
            this.tbxAdresseMail = new System.Windows.Forms.TextBox();
            this.tbxTel = new System.Windows.Forms.TextBox();
            this.tbxPrenom = new System.Windows.Forms.TextBox();
            this.tbxNom = new System.Windows.Forms.TextBox();
            this.lblDisciplineLigue = new System.Windows.Forms.Label();
            this.cbLigues = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Noto Sans Cond", 36F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(97, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(377, 65);
            this.label6.TabIndex = 11;
            this.label6.Text = "Créer un membre";
            // 
            // btnCreerMembre
            // 
            this.btnCreerMembre.Font = new System.Drawing.Font("Noto Sans Cond", 12F, System.Drawing.FontStyle.Bold);
            this.btnCreerMembre.Location = new System.Drawing.Point(177, 353);
            this.btnCreerMembre.Name = "btnCreerMembre";
            this.btnCreerMembre.Size = new System.Drawing.Size(126, 64);
            this.btnCreerMembre.TabIndex = 22;
            this.btnCreerMembre.Text = "Créer ";
            this.btnCreerMembre.UseVisualStyleBackColor = true;
            // 
            // lblVilleLigue
            // 
            this.lblVilleLigue.AutoSize = true;
            this.lblVilleLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblVilleLigue.Location = new System.Drawing.Point(59, 251);
            this.lblVilleLigue.Name = "lblVilleLigue";
            this.lblVilleLigue.Size = new System.Drawing.Size(114, 26);
            this.lblVilleLigue.TabIndex = 20;
            this.lblVilleLigue.Text = "Adresse Mail";
            // 
            // lblCodePostalLigue
            // 
            this.lblCodePostalLigue.AutoSize = true;
            this.lblCodePostalLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblCodePostalLigue.Location = new System.Drawing.Point(59, 209);
            this.lblCodePostalLigue.Name = "lblCodePostalLigue";
            this.lblCodePostalLigue.Size = new System.Drawing.Size(92, 26);
            this.lblCodePostalLigue.TabIndex = 19;
            this.lblCodePostalLigue.Text = "Téléphone";
            // 
            // lblAdresseLigue
            // 
            this.lblAdresseLigue.AutoSize = true;
            this.lblAdresseLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblAdresseLigue.Location = new System.Drawing.Point(59, 163);
            this.lblAdresseLigue.Name = "lblAdresseLigue";
            this.lblAdresseLigue.Size = new System.Drawing.Size(74, 26);
            this.lblAdresseLigue.TabIndex = 18;
            this.lblAdresseLigue.Text = "Prénom";
            // 
            // lblNomLigue
            // 
            this.lblNomLigue.AutoSize = true;
            this.lblNomLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblNomLigue.Location = new System.Drawing.Point(59, 120);
            this.lblNomLigue.Name = "lblNomLigue";
            this.lblNomLigue.Size = new System.Drawing.Size(51, 26);
            this.lblNomLigue.TabIndex = 17;
            this.lblNomLigue.Text = "Nom";
            // 
            // tbxAdresseMail
            // 
            this.tbxAdresseMail.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxAdresseMail.Location = new System.Drawing.Point(211, 248);
            this.tbxAdresseMail.Name = "tbxAdresseMail";
            this.tbxAdresseMail.Size = new System.Drawing.Size(278, 33);
            this.tbxAdresseMail.TabIndex = 15;
            this.tbxAdresseMail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAdresseMail_KeyPress);
            this.tbxAdresseMail.Validating += new System.ComponentModel.CancelEventHandler(this.tbxAdresseMail_Validating);
            // 
            // tbxTel
            // 
            this.tbxTel.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxTel.Location = new System.Drawing.Point(211, 206);
            this.tbxTel.Name = "tbxTel";
            this.tbxTel.Size = new System.Drawing.Size(278, 33);
            this.tbxTel.TabIndex = 14;
            this.tbxTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxTel_KeyPress);
            this.tbxTel.Validating += new System.ComponentModel.CancelEventHandler(this.tbxTel_Validating);
            // 
            // tbxPrenom
            // 
            this.tbxPrenom.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxPrenom.Location = new System.Drawing.Point(211, 160);
            this.tbxPrenom.Name = "tbxPrenom";
            this.tbxPrenom.Size = new System.Drawing.Size(278, 33);
            this.tbxPrenom.TabIndex = 13;
            this.tbxPrenom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxPrenom_KeyPress);
            this.tbxPrenom.Validating += new System.ComponentModel.CancelEventHandler(this.tbxPrenom_Validating);
            // 
            // tbxNom
            // 
            this.tbxNom.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxNom.Location = new System.Drawing.Point(211, 117);
            this.tbxNom.Name = "tbxNom";
            this.tbxNom.Size = new System.Drawing.Size(278, 33);
            this.tbxNom.TabIndex = 12;
            this.tbxNom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNom_KeyPress);
            this.tbxNom.Validating += new System.ComponentModel.CancelEventHandler(this.tbxNom_Validating);
            // 
            // lblDisciplineLigue
            // 
            this.lblDisciplineLigue.AutoSize = true;
            this.lblDisciplineLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblDisciplineLigue.Location = new System.Drawing.Point(59, 295);
            this.lblDisciplineLigue.Name = "lblDisciplineLigue";
            this.lblDisciplineLigue.Size = new System.Drawing.Size(55, 26);
            this.lblDisciplineLigue.TabIndex = 21;
            this.lblDisciplineLigue.Text = "Ligue";
            // 
            // cbLigues
            // 
            this.cbLigues.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.cbLigues.FormattingEnabled = true;
            this.cbLigues.Location = new System.Drawing.Point(211, 292);
            this.cbLigues.Name = "cbLigues";
            this.cbLigues.Size = new System.Drawing.Size(278, 34);
            this.cbLigues.TabIndex = 23;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmAjoutMembres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 450);
            this.Controls.Add(this.cbLigues);
            this.Controls.Add(this.btnCreerMembre);
            this.Controls.Add(this.lblDisciplineLigue);
            this.Controls.Add(this.lblVilleLigue);
            this.Controls.Add(this.lblCodePostalLigue);
            this.Controls.Add(this.lblAdresseLigue);
            this.Controls.Add(this.lblNomLigue);
            this.Controls.Add(this.tbxAdresseMail);
            this.Controls.Add(this.tbxTel);
            this.Controls.Add(this.tbxPrenom);
            this.Controls.Add(this.tbxNom);
            this.Controls.Add(this.label6);
            this.Name = "frmAjoutMembres";
            this.Text = "frmAjoutMembres";
            this.Load += new System.EventHandler(this.frmAjoutMembres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCreerMembre;
        private System.Windows.Forms.Label lblVilleLigue;
        private System.Windows.Forms.Label lblCodePostalLigue;
        private System.Windows.Forms.Label lblAdresseLigue;
        private System.Windows.Forms.Label lblNomLigue;
        private System.Windows.Forms.TextBox tbxAdresseMail;
        private System.Windows.Forms.TextBox tbxTel;
        private System.Windows.Forms.TextBox tbxPrenom;
        private System.Windows.Forms.TextBox tbxNom;
        private System.Windows.Forms.Label lblDisciplineLigue;
        private System.Windows.Forms.ComboBox cbLigues;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}