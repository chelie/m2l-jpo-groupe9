﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jpo
{
    public partial class frmEnregistrementMembres : Form
    {
        public frmEnregistrementMembres()
        {
            InitializeComponent();
        }

        private void frmEnregistrementMembres_Load(object sender, EventArgs e)
        {
            loadLigues();

            btnDesinscription.Enabled = false;
            btnModification.Enabled = false;
        }

        private void btnDesinscription_Click(object sender, EventArgs e)
        {
            int codeMembre = getCodeMembre();
            DbConnex.connexionBase();

            String sql = "DELETE FROM membre WHERE codeMembre=" + codeMembre;
            DbConnex.nonQuery(sql);
            DbConnex.connexionClose();
            lbMembres.Items.Remove(lbMembres.SelectedItem);
        }

        private void lbLigues_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadMembres();
            btnDesinscription.Enabled = false;
            btnModification.Enabled = false;
        }

        private void lbMembres_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnDesinscription.Enabled = true;
            btnModification.Enabled = true;
        }

        private void btnInscription_Click(object sender, EventArgs e)
        {
            frmAjoutMembres formAjtMembre = new frmAjoutMembres();
            formAjtMembre.FormClosed += new FormClosedEventHandler(formAJtMembre_Closed);
            formAjtMembre.Show();

        }

        private void formAJtMembre_Closed(object sender, EventArgs e)
        {
            loadMembres();
        }

        // FONCTIONS-------------------------------------------------------------

        private int getCodeLigue()
        {
            int codeLigue = -1;
            DbConnex.connexionBase();
            OleDbDataReader getCodeLigue = DbConnex.GetDataReader("select codeLigue from ligues where nomLigue ='" + lbLigues.SelectedItem + "'");
            while (getCodeLigue.Read())
            {
                codeLigue = Convert.ToInt32(getCodeLigue[0]);
            }
            DbConnex.connexionClose();

            return codeLigue;
        }

        private void loadMembres()
        {
            lbMembres.Items.Clear();
            int codeLigue = getCodeLigue();
            DbConnex.connexionBase();

            OleDbDataReader drLigues1 = DbConnex.GetDataReader("select * from membre where codeLigue=" + codeLigue);
            while (drLigues1.Read())
            {
                lbMembres.Items.Add(drLigues1[1]);
            }

            DbConnex.connexionClose();
        }

        private int getCodeMembre()
        {
            int codeMembre = -1;
            DbConnex.connexionBase();
            OleDbDataReader sql = DbConnex.GetDataReader("SELECT codeMembre FROM membre WHERE nom='" + lbMembres.SelectedItem.ToString()+"'");
            while (sql.Read())
            {
                codeMembre = Convert.ToInt16(sql[0]);
            }
            DbConnex.connexionClose();
            return codeMembre;
        }
        private void loadLigues()
        {
            lbLigues.Items.Clear();
            DbConnex.connexionBase();

            OleDbDataReader drLigues1 = DbConnex.GetDataReader("select * from ligues ");
            while (drLigues1.Read())
            {
                lbLigues.Items.Add(drLigues1[1]);
            }

            DbConnex.connexionClose();
        }
    }
}
