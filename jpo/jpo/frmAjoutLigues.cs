﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jpo
{
    public partial class frmAjoutLigues : Form
    {
        public frmAjoutLigues()
        {
            InitializeComponent();
        }

        private void frmAjoutLigues_Load(object sender, EventArgs e)
        {
            AutoValidate = AutoValidate.Disable;
        }

        private void btnCreerLigue_Click(object sender, EventArgs e)
        {
            var valid = ValidateChildren();
            
            if (valid)
            {
                String sql = "insert into ligues (nomLigue,adresse,cp,ville,discipline)" +
                        " values ('" + tbxNomLigue.Text +
                        "','" + tbxAdresse.Text +
                        "','" + tbxCodePostal.Text +
                        "','" + tbxVille.Text +
                        "','" + tbxDiscipline.Text + "')";
                if (DbConnex.etatConnection() != ConnectionState.Open)
                {
                    DbConnex.connexionBase();
                }
                int cmd = DbConnex.nonQuery(sql);
                if (cmd != 1) { MessageBox.Show("Une erreur s'est produite, veuillez vérifier que le nom de ligue ou la discipline n'est pas déjà utilisé."); } 
                else
                {
                    DbConnex.connexionClose();
                    this.Close();
                }
            }
        }

        private void tbxNomLigue_Validating(object sender, CancelEventArgs e)
        {
            if (tbxNomLigue.Text == "")
            {
                this.errorProvider1.SetError(tbxNomLigue, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxAdresse_Validating(object sender, CancelEventArgs e)
        {
            if (tbxAdresse.Text == "")
            {
                this.errorProvider1.SetError(tbxAdresse, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxCodePostal_Validating(object sender, CancelEventArgs e)
        {
            if (tbxCodePostal.Text == "")
            {
                this.errorProvider1.SetError(tbxCodePostal, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxVille_Validating(object sender, CancelEventArgs e)
        {
            if (tbxVille.Text == "")
            {
                this.errorProvider1.SetError(tbxVille, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxDiscipline_Validating(object sender, CancelEventArgs e)
        {
            if (tbxDiscipline.Text == "")
            {
                this.errorProvider1.SetError(tbxDiscipline, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxNomLigue_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxNomLigue.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxAdresse_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxAdresse.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void tbxCodePostal_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxCodePostal.MaxLength = 5;
            var regex = new Regex(@"[^0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char) Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void tbxVille_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxVille.MaxLength = 50;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxDiscipline_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxDiscipline.MaxLength = 50;    
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
