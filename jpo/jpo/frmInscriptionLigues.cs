﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace jpo
{
    public partial class frmInscriptionLigues : Form
    {
        static List<Equipement> listeEquipement = new List<Equipement>();
        static List<ColoredItem> listeLigue = new List<ColoredItem>();

        public frmInscriptionLigues()
        {
            InitializeComponent();
        }

        private void frmInscriptionLigues_Load(object sender, EventArgs e)
        {
            etatBase();
            btnAnnuler.Hide();
            btnConfirmModif.Hide();

            lbLigues.DrawMode = DrawMode.OwnerDrawFixed;

            loadLigues();
            loadEquipement();
        }

        private void lbLigues_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = lbLigues.SelectedItem as ColoredItem;

            clbClear();

            if (!selectedItem.Color.Equals(Color.Green))
            {
                etatNonInscrit();
            }
            else
            {
                etatInscrit();

            }
        }

        private void lbLigues_DrawItem(object sender, DrawItemEventArgs e)
        {
            var item = lbLigues.Items[e.Index] as ColoredItem;

            if (item != null)
            {
                e.DrawBackground();
                e.Graphics.DrawString(
                    item.Text,
                    e.Font,
                    new SolidBrush(item.Color),
                    e.Bounds);
            }
        }

        private void btnInscription_Click(object sender, EventArgs e)
        {
            if(clbFournitures.CheckedIndices.Count == 0)
            {
                MessageBox.Show("Le formulaire n'est pas rempli.");
                return;
            }
            createInscription();
            insertEquipement();
            loadLigues();
            clearForm();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            etatModif();
        }

        private void btnDesinscription_Click(object sender, EventArgs e)
        {
            deleteInscription();
            deleteDemandes();
            loadLigues();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            clearForm();
        }

        private void btnConfirmModif_Click(object sender, EventArgs e)
        {
            deleteDemandes();
            insertEquipement();
            updateLength();
            clearForm();
        }

        private void tbxLongueur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxLargeur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxRecherche_TextChanged(object sender, EventArgs e)
        {
            lbLigues.Items.Clear();
            foreach (ColoredItem item in listeLigue)
            {
                string ligue = item.Text;
                if (ligue.Contains(tbxRecherche.Text.ToLower()))
                {
                    lbLigues.Items.Add(item);
                }
            }
        }


        // FUNCTIONS ---------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void createInscription()
        {
            int codeLigue = getCodeLigue();
            DbConnex.connexionBase();
            String sql = "insert into inscription (longueur,largeur,codeligue)" +
                    " values (" + tbxLongueur.Text + "," + tbxLargeur.Text + "," + codeLigue + ")";
            DbConnex.nonQuery(sql);
            DbConnex.connexionClose();
        }

        private void deleteInscription()
        {
            int numInscription = getNumInscription();
            DbConnex.connexionBase();
            String sql = "DELETE FROM inscription WHERE numInscription=" + numInscription + "";
            DbConnex.nonQuery(sql);
            DbConnex.connexionClose();
        }

        private void updateLength()
        {
            int numInscription = getNumInscription();
            DbConnex.connexionBase();
            String sql = "UPDATE inscription SET longueur=" + tbxLongueur.Text + ",largeur=" + tbxLargeur.Text + " WHERE numInscription=" + numInscription;
            DbConnex.nonQuery(sql);
            DbConnex.connexionClose();
        }

        private void deleteDemandes()
        {
            int numInscription = getNumInscription();
            DbConnex.connexionBase();
            String sql = "DELETE FROM demander WHERE numInscription=" + numInscription + "";
            DbConnex.nonQuery(sql);
            DbConnex.connexionClose();
        }

        private void loadLigues()
        {
            lbLigues.Items.Clear();
            DbConnex.connexionBase();

            OleDbDataReader drLigues1 = DbConnex.GetDataReader("select * from ligues where codeLigue not in (select codeligue from inscription)");
            while (drLigues1.Read())
            {
                ColoredItem ci = new ColoredItem { Color = Color.Red, Text = drLigues1.GetString(1) };
                listeLigue.Add(ci);
                lbLigues.Items.Add(ci);
            }

            OleDbDataReader drLigues = DbConnex.GetDataReader("select * from ligues,inscription where ligues.codeLigue = inscription.codeligue");
            while (drLigues.Read())
            {
                ColoredItem ci = new ColoredItem { Color = Color.Green, Text = drLigues.GetString(1) };
                listeLigue.Add(ci);
                lbLigues.Items.Add(ci);
            }

            DbConnex.connexionClose();
        }

        private void loadEquipement()
        {
            clbFournitures.Items.Clear();
            DbConnex.connexionBase();
            OleDbDataReader getCodeLigue = DbConnex.GetDataReader("select * from equipement");
            while (getCodeLigue.Read())
            {
                var equipement = new Equipement(Convert.ToInt16(getCodeLigue[0]), Convert.ToString(getCodeLigue[1]));
                listeEquipement.Add(equipement);
                clbFournitures.Items.Add(equipement.libelle);
            }
            DbConnex.connexionClose();
        }
        private void clbClear()
        {
            for (int i = 0; i < clbFournitures.Items.Count; i++)
            {
                clbFournitures.SetSelected(i, false);
                clbFournitures.SetItemChecked(i, false);
            }
            tbxLongueur.Text = string.Empty;
            tbxLargeur.Text = string.Empty;
            clbFournitures.Enabled = false;
        }
        private void clearForm()
        {
            lbLigues.SelectedItem = 0;
            tbxLargeur.Enabled = false;
            tbxLongueur.Enabled = false;
            lbLigues.Enabled = true;
            btnDesinscription.Enabled = true;
            btnAnnuler.Hide();
            btnConfirmModif.Hide();
        }

        private int getCodeLigue()
        {
            int codeLigue = -1;
            DbConnex.connexionBase();
            var Ligue = lbLigues.SelectedItem as ColoredItem;
            OleDbDataReader getCodeLigue = DbConnex.GetDataReader("select codeLigue from ligues where nomLigue ='" + Ligue.Text + "'");
            while (getCodeLigue.Read())
            {
                codeLigue = Convert.ToInt32(getCodeLigue[0]);
            }
            DbConnex.connexionClose();

            return codeLigue;
        }

        private int getNumInscription()
        {
            int numInscription = -1;
            int codeLigue = getCodeLigue();
            DbConnex.connexionBase();
            OleDbDataReader sql = DbConnex.GetDataReader("select numInscription from inscription where codeLigue =" + codeLigue + "");
            while (sql.Read())
            {
                numInscription = Convert.ToInt32(sql[0]);
            }
            DbConnex.connexionClose();

            return numInscription;
        }

        private void insertEquipement()
        {
            int numInscription = getNumInscription();
            DbConnex.connexionBase();
            foreach (var checkedEquipement in clbFournitures.CheckedItems)
            {
                foreach (Equipement equipement in listeEquipement)
                {
                    if (equipement.libelle.ToLower() == clbFournitures.GetItemText(checkedEquipement).ToLower())
                    {
                        String sqlDemander = "insert into demander (numInscription,codeEquipement)" +
                                " values (" + numInscription + "," + equipement.code + ")";
                        DbConnex.nonQuery(sqlDemander);
                    }
                }
            }
            DbConnex.connexionClose();
        }

        // Liste des états du Form
        private void etatBase()
        {

            tbxLargeur.Enabled = false;
            tbxLongueur.Enabled = false;
            clbFournitures.Enabled = false;
            btnModifier.Enabled = false;
            btnDesinscription.Enabled = false;
            btnInscription.Enabled = false;
            
        }

        private void etatInscrit()
        {
            etatBase();
            btnDesinscription.Enabled = true;
            btnModifier.Enabled = true;
            loadLigueInfo();
        }

        private void etatNonInscrit()
        {
            etatBase();
            btnInscription.Enabled = true;
            tbxLongueur.Enabled = true;
            tbxLargeur.Enabled = true;
            clbFournitures.Enabled = true;
        }

        private void etatModif()
        {
            btnAnnuler.Show();
            btnConfirmModif.Show();
            btnDesinscription.Enabled = false;
            lbLigues.Enabled = false;
            tbxLongueur.Enabled = true;
            tbxLargeur.Enabled = true;
            clbFournitures.Enabled = true;
        }

        private void loadLigueInfo()
        {
            int numInscription = getNumInscription();
            DbConnex.connexionBase();
            OleDbDataReader getChecked = DbConnex.GetDataReader("SELECT codeEquipement FROM demander WHERE numInscription=" + numInscription);
            while (getChecked.Read())
            {
                for (int i = 0; i < clbFournitures.Items.Count; i++)
                {
                    if (Convert.ToInt16(getChecked[0]) == listeEquipement[i].code)
                    {
                        clbFournitures.SetSelected(i, false);
                        clbFournitures.SetItemChecked(i, true);
                    }
                }
            }
            OleDbDataReader getLength = DbConnex.GetDataReader("SELECT longueur,largeur FROM inscription WHERE numInscription=" + numInscription);
            while (getLength.Read())
            {
                tbxLongueur.Text = Convert.ToString(getLength[0]);
                tbxLargeur.Text = Convert.ToString(getLength[1]);
            }
            DbConnex.connexionClose();
        }
    }
    class ColoredItem
    {
        public string Text { get; set; }
        public Color Color { get; set; }
    };

    class Equipement
    {
        public int code { get; set; }
        public string libelle { get; set; }

        public Equipement(int code, string libelle)
        {
            this.code = code;
            this.libelle = libelle;
        }
    }
}
