﻿
namespace jpo
{
    partial class frmInscriptionLigues
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbLigues = new System.Windows.Forms.ListBox();
            this.clbFournitures = new System.Windows.Forms.CheckedListBox();
            this.btnInscription = new System.Windows.Forms.Button();
            this.tbxLargeur = new System.Windows.Forms.TextBox();
            this.tbxLongueur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDesinscription = new System.Windows.Forms.Button();
            this.btnModifier = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnConfirmModif = new System.Windows.Forms.Button();
            this.tbxRecherche = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Noto Sans Cond", 36F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(-7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(814, 69);
            this.label1.TabIndex = 1;
            this.label1.Text = "Inscription Ligue";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbLigues
            // 
            this.lbLigues.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lbLigues.FormattingEnabled = true;
            this.lbLigues.HorizontalScrollbar = true;
            this.lbLigues.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbLigues.ItemHeight = 26;
            this.lbLigues.Location = new System.Drawing.Point(24, 148);
            this.lbLigues.Name = "lbLigues";
            this.lbLigues.Size = new System.Drawing.Size(296, 290);
            this.lbLigues.TabIndex = 5;
            this.lbLigues.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbLigues_DrawItem);
            this.lbLigues.SelectedIndexChanged += new System.EventHandler(this.lbLigues_SelectedIndexChanged);
            // 
            // clbFournitures
            // 
            this.clbFournitures.BackColor = System.Drawing.SystemColors.Window;
            this.clbFournitures.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.clbFournitures.FormattingEnabled = true;
            this.clbFournitures.Location = new System.Drawing.Point(509, 210);
            this.clbFournitures.Name = "clbFournitures";
            this.clbFournitures.Size = new System.Drawing.Size(274, 228);
            this.clbFournitures.TabIndex = 8;
            // 
            // btnInscription
            // 
            this.btnInscription.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnInscription.Location = new System.Drawing.Point(351, 148);
            this.btnInscription.Name = "btnInscription";
            this.btnInscription.Size = new System.Drawing.Size(138, 66);
            this.btnInscription.TabIndex = 9;
            this.btnInscription.Text = "Inscription";
            this.btnInscription.UseVisualStyleBackColor = true;
            this.btnInscription.Click += new System.EventHandler(this.btnInscription_Click);
            // 
            // tbxLargeur
            // 
            this.tbxLargeur.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxLargeur.Location = new System.Drawing.Point(669, 159);
            this.tbxLargeur.Name = "tbxLargeur";
            this.tbxLargeur.Size = new System.Drawing.Size(114, 33);
            this.tbxLargeur.TabIndex = 10;
            this.tbxLargeur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxLargeur_KeyPress);
            // 
            // tbxLongueur
            // 
            this.tbxLongueur.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxLongueur.Location = new System.Drawing.Point(509, 159);
            this.tbxLongueur.Name = "tbxLongueur";
            this.tbxLongueur.Size = new System.Drawing.Size(127, 33);
            this.tbxLongueur.TabIndex = 11;
            this.tbxLongueur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxLongueur_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(509, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 26);
            this.label2.TabIndex = 12;
            this.label2.Text = "Longueur (cm)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(669, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 26);
            this.label3.TabIndex = 13;
            this.label3.Text = "Largeur (cm)";
            // 
            // btnDesinscription
            // 
            this.btnDesinscription.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnDesinscription.Location = new System.Drawing.Point(351, 220);
            this.btnDesinscription.Name = "btnDesinscription";
            this.btnDesinscription.Size = new System.Drawing.Size(138, 66);
            this.btnDesinscription.TabIndex = 14;
            this.btnDesinscription.Text = "Désinscription";
            this.btnDesinscription.UseVisualStyleBackColor = true;
            this.btnDesinscription.Click += new System.EventHandler(this.btnDesinscription_Click);
            // 
            // btnModifier
            // 
            this.btnModifier.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnModifier.Location = new System.Drawing.Point(351, 292);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(138, 66);
            this.btnModifier.TabIndex = 15;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnAnnuler.Location = new System.Drawing.Point(351, 364);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(138, 63);
            this.btnAnnuler.TabIndex = 16;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnConfirmModif
            // 
            this.btnConfirmModif.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnConfirmModif.Location = new System.Drawing.Point(351, 292);
            this.btnConfirmModif.Name = "btnConfirmModif";
            this.btnConfirmModif.Size = new System.Drawing.Size(138, 66);
            this.btnConfirmModif.TabIndex = 17;
            this.btnConfirmModif.Text = "Confirmer Modification";
            this.btnConfirmModif.UseVisualStyleBackColor = true;
            this.btnConfirmModif.Click += new System.EventHandler(this.btnConfirmModif_Click);
            // 
            // tbxRecherche
            // 
            this.tbxRecherche.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxRecherche.Location = new System.Drawing.Point(24, 96);
            this.tbxRecherche.Name = "tbxRecherche";
            this.tbxRecherche.Size = new System.Drawing.Size(296, 33);
            this.tbxRecherche.TabIndex = 39;
            this.tbxRecherche.TextChanged += new System.EventHandler(this.tbxRecherche_TextChanged);
            // 
            // frmInscriptionLigues
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbxRecherche);
            this.Controls.Add(this.btnConfirmModif);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.btnDesinscription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxLongueur);
            this.Controls.Add(this.tbxLargeur);
            this.Controls.Add(this.btnInscription);
            this.Controls.Add(this.clbFournitures);
            this.Controls.Add(this.lbLigues);
            this.Controls.Add(this.label1);
            this.Name = "frmInscriptionLigues";
            this.Text = "Inscription ligues";
            this.Load += new System.EventHandler(this.frmInscriptionLigues_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbLigues;
        private System.Windows.Forms.CheckedListBox clbFournitures;
        private System.Windows.Forms.Button btnInscription;
        private System.Windows.Forms.TextBox tbxLargeur;
        private System.Windows.Forms.TextBox tbxLongueur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDesinscription;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnConfirmModif;
        private System.Windows.Forms.TextBox tbxRecherche;
    }
}