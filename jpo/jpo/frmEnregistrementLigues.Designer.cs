﻿
namespace jpo
{
    partial class frmEnregistrementLigues
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.tbxAdresse = new System.Windows.Forms.TextBox();
            this.tbxCodePostal = new System.Windows.Forms.TextBox();
            this.tbxVille = new System.Windows.Forms.TextBox();
            this.tbxDiscipline = new System.Windows.Forms.TextBox();
            this.tbxNomLigue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnModifier = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.lbLigues = new System.Windows.Forms.ListBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnConfirmModif = new System.Windows.Forms.Button();
            this.tbxRecherche = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnSupprimer.Location = new System.Drawing.Point(795, 435);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(142, 60);
            this.btnSupprimer.TabIndex = 31;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // tbxAdresse
            // 
            this.tbxAdresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAdresse.Location = new System.Drawing.Point(614, 193);
            this.tbxAdresse.Name = "tbxAdresse";
            this.tbxAdresse.Size = new System.Drawing.Size(305, 31);
            this.tbxAdresse.TabIndex = 30;
            this.tbxAdresse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAdresse_KeyPress);
            this.tbxAdresse.Validating += new System.ComponentModel.CancelEventHandler(this.tbxAdresse_Validating);
            // 
            // tbxCodePostal
            // 
            this.tbxCodePostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCodePostal.Location = new System.Drawing.Point(614, 241);
            this.tbxCodePostal.Name = "tbxCodePostal";
            this.tbxCodePostal.Size = new System.Drawing.Size(305, 31);
            this.tbxCodePostal.TabIndex = 29;
            this.tbxCodePostal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxCodePostal_KeyPress);
            this.tbxCodePostal.Validating += new System.ComponentModel.CancelEventHandler(this.tbxCodePostal_Validating);
            // 
            // tbxVille
            // 
            this.tbxVille.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxVille.Location = new System.Drawing.Point(614, 292);
            this.tbxVille.Name = "tbxVille";
            this.tbxVille.Size = new System.Drawing.Size(305, 31);
            this.tbxVille.TabIndex = 28;
            this.tbxVille.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxVille_KeyPress);
            this.tbxVille.Validating += new System.ComponentModel.CancelEventHandler(this.tbxVille_Validating);
            // 
            // tbxDiscipline
            // 
            this.tbxDiscipline.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDiscipline.Location = new System.Drawing.Point(614, 341);
            this.tbxDiscipline.Name = "tbxDiscipline";
            this.tbxDiscipline.Size = new System.Drawing.Size(305, 31);
            this.tbxDiscipline.TabIndex = 27;
            this.tbxDiscipline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDiscipline_KeyPress);
            this.tbxDiscipline.Validating += new System.ComponentModel.CancelEventHandler(this.tbxDiscipline_Validating);
            // 
            // tbxNomLigue
            // 
            this.tbxNomLigue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNomLigue.Location = new System.Drawing.Point(614, 149);
            this.tbxNomLigue.Name = "tbxNomLigue";
            this.tbxNomLigue.Size = new System.Drawing.Size(305, 31);
            this.tbxNomLigue.TabIndex = 26;
            this.tbxNomLigue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNomLigue_KeyPress);
            this.tbxNomLigue.Validating += new System.ComponentModel.CancelEventHandler(this.tbxNomLigue_Validating);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(487, 344);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(245, 25);
            this.label6.TabIndex = 25;
            this.label6.Tag = "";
            this.label6.Text = "Discipline";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(487, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(245, 25);
            this.label5.TabIndex = 24;
            this.label5.Tag = "";
            this.label5.Text = "Ville";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(487, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(245, 25);
            this.label4.TabIndex = 23;
            this.label4.Tag = "";
            this.label4.Text = "Code Postal";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(487, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(245, 25);
            this.label3.TabIndex = 22;
            this.label3.Tag = "";
            this.label3.Text = "Adresse";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(487, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(255, 25);
            this.label2.TabIndex = 21;
            this.label2.Tag = "";
            this.label2.Text = "Nom de ligue";
            // 
            // btnModifier
            // 
            this.btnModifier.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnModifier.Location = new System.Drawing.Point(647, 436);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(142, 60);
            this.btnModifier.TabIndex = 19;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Noto Sans Cond", 36F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(70, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(820, 68);
            this.label1.TabIndex = 18;
            this.label1.Text = "Enregistrement Ligue";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnEnregistrer.Location = new System.Drawing.Point(499, 436);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(142, 60);
            this.btnEnregistrer.TabIndex = 34;
            this.btnEnregistrer.Text = "Créer une ligue";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // lbLigues
            // 
            this.lbLigues.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lbLigues.FormattingEnabled = true;
            this.lbLigues.ItemHeight = 26;
            this.lbLigues.Location = new System.Drawing.Point(28, 154);
            this.lbLigues.Name = "lbLigues";
            this.lbLigues.Size = new System.Drawing.Size(262, 342);
            this.lbLigues.TabIndex = 35;
            this.lbLigues.SelectedIndexChanged += new System.EventHandler(this.lbLigues_SelectedIndexChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnAnnuler.Location = new System.Drawing.Point(499, 435);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(142, 60);
            this.btnAnnuler.TabIndex = 36;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Visible = false;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnConfirmModif
            // 
            this.btnConfirmModif.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.btnConfirmModif.Location = new System.Drawing.Point(647, 435);
            this.btnConfirmModif.Name = "btnConfirmModif";
            this.btnConfirmModif.Size = new System.Drawing.Size(142, 60);
            this.btnConfirmModif.TabIndex = 37;
            this.btnConfirmModif.Text = "Confirmer la modification";
            this.btnConfirmModif.UseVisualStyleBackColor = true;
            this.btnConfirmModif.Visible = false;
            this.btnConfirmModif.Click += new System.EventHandler(this.btnConfirmModif_Click);
            // 
            // tbxRecherche
            // 
            this.tbxRecherche.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxRecherche.Location = new System.Drawing.Point(28, 104);
            this.tbxRecherche.Name = "tbxRecherche";
            this.tbxRecherche.Size = new System.Drawing.Size(262, 33);
            this.tbxRecherche.TabIndex = 38;
            this.tbxRecherche.TextChanged += new System.EventHandler(this.tbxRecherche_TextChanged);
            // 
            // frmEnregistrementLigues
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 524);
            this.Controls.Add(this.tbxRecherche);
            this.Controls.Add(this.btnConfirmModif);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.lbLigues);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.tbxAdresse);
            this.Controls.Add(this.tbxCodePostal);
            this.Controls.Add(this.tbxVille);
            this.Controls.Add(this.tbxDiscipline);
            this.Controls.Add(this.tbxNomLigue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.label1);
            this.Name = "frmEnregistrementLigues";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enregistrement ligues";
            this.Load += new System.EventHandler(this.frmEnregistrementLigues_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.TextBox tbxAdresse;
        private System.Windows.Forms.TextBox tbxCodePostal;
        private System.Windows.Forms.TextBox tbxVille;
        private System.Windows.Forms.TextBox tbxDiscipline;
        private System.Windows.Forms.TextBox tbxNomLigue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.ListBox lbLigues;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnConfirmModif;
        private System.Windows.Forms.TextBox tbxRecherche;
    }
}