﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jpo
{
    public partial class frmAjoutMembres : Form
    {
        public frmAjoutMembres()
        {
            InitializeComponent();

        }

        private void frmAjoutMembres_Load(object sender, EventArgs e)
        {
            loadLigues();
        }

        private void loadLigues()
        {
            DbConnex.connexionBase();

            OleDbDataReader drLigues1 = DbConnex.GetDataReader("select * from ligues ");
            while (drLigues1.Read())
            {
                cbLigues.Items.Add(drLigues1[1]);
            }

            DbConnex.connexionClose();
        }

        private void btnCreerMembre_Click(object sender, EventArgs e)
        {
            int codeLigue = getCodeLigue(cbLigues.SelectedItem.ToString());
            var valid = ValidateChildren();

            if (valid)
            {
                String sql = "insert into membre (nom,prénom,téléphone,mail,codeLigue)" +
                        " values ('" + tbxNom.Text +
                        "','" + tbxPrenom.Text +
                        "','" + tbxTel.Text +
                        "','" + tbxAdresseMail.Text +
                        "','" + codeLigue + "')";
                if (DbConnex.etatConnection() != ConnectionState.Open)
                {
                    DbConnex.connexionBase();
                }
                int cmd = DbConnex.nonQuery(sql);
                if (cmd != 1) { MessageBox.Show("Une erreur s'est produite, veuillez vérifier que le nom de ligue ou la discipline n'est pas déjà utilisé."); }
                else
                {
                    DbConnex.connexionClose();
                    this.Close();
                }
            }
        }

        private int getCodeLigue(String s)
        {
            int codeLigue = -1;
            DbConnex.connexionBase();
            OleDbDataReader getCodeLigue = DbConnex.GetDataReader("select codeLigue from ligues where nomLigue ='" + s + "'");
            while (getCodeLigue.Read())
            {
                codeLigue = Convert.ToInt32(getCodeLigue[0]);
            }
            DbConnex.connexionClose();

            return codeLigue;
        }

        private void tbxNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxNom.MaxLength = 40;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void tbxPrenom_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxPrenom.MaxLength = 40;
            var regex = new Regex(@"[^a-zA-Z\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void tbxTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxTel.MaxLength = 20;
            var regex = new Regex(@"[^0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void tbxAdresseMail_KeyPress(object sender, KeyPressEventArgs e)
        {
            tbxAdresseMail.MaxLength = 40;
        }

        private void tbxNom_Validating(object sender, CancelEventArgs e)
        {
            if (tbxNom.Text == "")
            {
                this.errorProvider1.SetError(tbxNom, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxPrenom_Validating(object sender, CancelEventArgs e)
        {
            if (tbxPrenom.Text == "")
            {
                this.errorProvider1.SetError(tbxPrenom, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxTel_Validating(object sender, CancelEventArgs e)
        {
            if (tbxTel.Text == "")
            {
                this.errorProvider1.SetError(tbxTel, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }

        private void tbxAdresseMail_Validating(object sender, CancelEventArgs e)
        {
            if (tbxAdresseMail.Text == "")
            {
                this.errorProvider1.SetError(tbxAdresseMail, "Vous devez remplir cette ligne.");
                e.Cancel = true;
            }
        }
    }
}
