﻿namespace jpo
{
    partial class frmAjoutLigues
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbxNomLigue = new System.Windows.Forms.TextBox();
            this.tbxAdresse = new System.Windows.Forms.TextBox();
            this.tbxCodePostal = new System.Windows.Forms.TextBox();
            this.tbxVille = new System.Windows.Forms.TextBox();
            this.tbxDiscipline = new System.Windows.Forms.TextBox();
            this.lblNomLigue = new System.Windows.Forms.Label();
            this.lblAdresseLigue = new System.Windows.Forms.Label();
            this.lblCodePostalLigue = new System.Windows.Forms.Label();
            this.lblVilleLigue = new System.Windows.Forms.Label();
            this.lblDisciplineLigue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCreerLigue = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxNomLigue
            // 
            this.tbxNomLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxNomLigue.Location = new System.Drawing.Point(202, 133);
            this.tbxNomLigue.Name = "tbxNomLigue";
            this.tbxNomLigue.Size = new System.Drawing.Size(278, 33);
            this.tbxNomLigue.TabIndex = 0;
            this.tbxNomLigue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxNomLigue_KeyPress);
            this.tbxNomLigue.Validating += new System.ComponentModel.CancelEventHandler(this.tbxNomLigue_Validating);
            // 
            // tbxAdresse
            // 
            this.tbxAdresse.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxAdresse.Location = new System.Drawing.Point(202, 176);
            this.tbxAdresse.Name = "tbxAdresse";
            this.tbxAdresse.Size = new System.Drawing.Size(278, 33);
            this.tbxAdresse.TabIndex = 1;
            this.tbxAdresse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxAdresse_KeyPress);
            this.tbxAdresse.Validating += new System.ComponentModel.CancelEventHandler(this.tbxAdresse_Validating);
            // 
            // tbxCodePostal
            // 
            this.tbxCodePostal.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxCodePostal.Location = new System.Drawing.Point(202, 222);
            this.tbxCodePostal.Name = "tbxCodePostal";
            this.tbxCodePostal.Size = new System.Drawing.Size(278, 33);
            this.tbxCodePostal.TabIndex = 2;
            this.tbxCodePostal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxCodePostal_KeyPress);
            this.tbxCodePostal.Validating += new System.ComponentModel.CancelEventHandler(this.tbxCodePostal_Validating);
            // 
            // tbxVille
            // 
            this.tbxVille.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxVille.Location = new System.Drawing.Point(202, 264);
            this.tbxVille.Name = "tbxVille";
            this.tbxVille.Size = new System.Drawing.Size(278, 33);
            this.tbxVille.TabIndex = 3;
            this.tbxVille.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxVille_KeyPress);
            this.tbxVille.Validating += new System.ComponentModel.CancelEventHandler(this.tbxVille_Validating);
            // 
            // tbxDiscipline
            // 
            this.tbxDiscipline.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.tbxDiscipline.Location = new System.Drawing.Point(202, 308);
            this.tbxDiscipline.Name = "tbxDiscipline";
            this.tbxDiscipline.Size = new System.Drawing.Size(278, 33);
            this.tbxDiscipline.TabIndex = 4;
            this.tbxDiscipline.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxDiscipline_KeyPress);
            this.tbxDiscipline.Validating += new System.ComponentModel.CancelEventHandler(this.tbxDiscipline_Validating);
            // 
            // lblNomLigue
            // 
            this.lblNomLigue.AutoSize = true;
            this.lblNomLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblNomLigue.Location = new System.Drawing.Point(50, 136);
            this.lblNomLigue.Name = "lblNomLigue";
            this.lblNomLigue.Size = new System.Drawing.Size(121, 26);
            this.lblNomLigue.TabIndex = 5;
            this.lblNomLigue.Text = "Nom de Ligue";
            // 
            // lblAdresseLigue
            // 
            this.lblAdresseLigue.AutoSize = true;
            this.lblAdresseLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblAdresseLigue.Location = new System.Drawing.Point(50, 179);
            this.lblAdresseLigue.Name = "lblAdresseLigue";
            this.lblAdresseLigue.Size = new System.Drawing.Size(74, 26);
            this.lblAdresseLigue.TabIndex = 6;
            this.lblAdresseLigue.Text = "Adresse";
            // 
            // lblCodePostalLigue
            // 
            this.lblCodePostalLigue.AutoSize = true;
            this.lblCodePostalLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblCodePostalLigue.Location = new System.Drawing.Point(50, 225);
            this.lblCodePostalLigue.Name = "lblCodePostalLigue";
            this.lblCodePostalLigue.Size = new System.Drawing.Size(105, 26);
            this.lblCodePostalLigue.TabIndex = 7;
            this.lblCodePostalLigue.Text = "Code Postal";
            // 
            // lblVilleLigue
            // 
            this.lblVilleLigue.AutoSize = true;
            this.lblVilleLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblVilleLigue.Location = new System.Drawing.Point(50, 267);
            this.lblVilleLigue.Name = "lblVilleLigue";
            this.lblVilleLigue.Size = new System.Drawing.Size(47, 26);
            this.lblVilleLigue.TabIndex = 8;
            this.lblVilleLigue.Text = "Ville";
            // 
            // lblDisciplineLigue
            // 
            this.lblDisciplineLigue.AutoSize = true;
            this.lblDisciplineLigue.Font = new System.Drawing.Font("Noto Sans Cond", 14F, System.Drawing.FontStyle.Bold);
            this.lblDisciplineLigue.Location = new System.Drawing.Point(50, 311);
            this.lblDisciplineLigue.Name = "lblDisciplineLigue";
            this.lblDisciplineLigue.Size = new System.Drawing.Size(89, 26);
            this.lblDisciplineLigue.TabIndex = 9;
            this.lblDisciplineLigue.Text = "Discipline";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Noto Sans Cond", 36F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(71, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(334, 65);
            this.label6.TabIndex = 10;
            this.label6.Text = "Créer une ligue";
            // 
            // btnCreerLigue
            // 
            this.btnCreerLigue.Font = new System.Drawing.Font("Noto Sans Cond", 12F, System.Drawing.FontStyle.Bold);
            this.btnCreerLigue.Location = new System.Drawing.Point(168, 369);
            this.btnCreerLigue.Name = "btnCreerLigue";
            this.btnCreerLigue.Size = new System.Drawing.Size(126, 64);
            this.btnCreerLigue.TabIndex = 11;
            this.btnCreerLigue.Text = "Créer ";
            this.btnCreerLigue.UseVisualStyleBackColor = true;
            this.btnCreerLigue.Click += new System.EventHandler(this.btnCreerLigue_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmAjoutLigues
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 461);
            this.Controls.Add(this.btnCreerLigue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblDisciplineLigue);
            this.Controls.Add(this.lblVilleLigue);
            this.Controls.Add(this.lblCodePostalLigue);
            this.Controls.Add(this.lblAdresseLigue);
            this.Controls.Add(this.lblNomLigue);
            this.Controls.Add(this.tbxDiscipline);
            this.Controls.Add(this.tbxVille);
            this.Controls.Add(this.tbxCodePostal);
            this.Controls.Add(this.tbxAdresse);
            this.Controls.Add(this.tbxNomLigue);
            this.Name = "frmAjoutLigues";
            this.Text = "frmAjoutLigues";
            this.Load += new System.EventHandler(this.frmAjoutLigues_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxNomLigue;
        private System.Windows.Forms.TextBox tbxAdresse;
        private System.Windows.Forms.TextBox tbxCodePostal;
        private System.Windows.Forms.TextBox tbxVille;
        private System.Windows.Forms.TextBox tbxDiscipline;
        private System.Windows.Forms.Label lblNomLigue;
        private System.Windows.Forms.Label lblAdresseLigue;
        private System.Windows.Forms.Label lblCodePostalLigue;
        private System.Windows.Forms.Label lblVilleLigue;
        private System.Windows.Forms.Label lblDisciplineLigue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCreerLigue;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}